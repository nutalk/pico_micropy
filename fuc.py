import time
import machine


def rjust(tag: str):
    tag = str(tag)
    if len(tag) == 1:
        return f'0{tag}'
    else:
        return tag


#Show time
def show_time(LCD, hour_adj=0, minute_adj=0, blink=True, y_adj=0, color=None):
    year, month, mday, hour, minute, second, weekday, yearday = time.localtime()
    hour += hour_adj
    while True:
        if hour >= 24:
            hour -= 24
        if hour < 0:
            hour += 24
        if 0 <= hour < 24:
            break
    minute += minute_adj
    while True:
        if minute >= 60:
            minute -= 60
        if minute < 0:
            minute += 60
        if 0 <= minute < 60:
            break

    hour_str = rjust(hour)
    minute_str = rjust(minute)
    if second % 2 == 0 and blink:
        strout = f'{hour_str} {minute_str}'
    else:
        strout = f'{hour_str}:{minute_str}'

    if color is None:
        color = LCD.black
    LCD.write_text(strout, 19, 100 + y_adj, 5, color)
    return year, month, mday, hour, minute, second, weekday, yearday


def adjust_time(LCD, Touch):
    Touch.Gestures = "None"
    adjust_rect = 1  # 0 for hour, 1 for minute
    hour_adj, minute_adj = 0, 0
    # not long press
    while Touch.Gestures != 0x0C:
        LCD.fill(LCD.white)
        show_time(LCD, hour_adj=hour_adj-1, minute_adj=minute_adj-1, blink=False, y_adj=-50, color=LCD.gray)
        year, month, mday, hour, minute, second, weekday, yearday = show_time(LCD, hour_adj=hour_adj, minute_adj=minute_adj, blink=False, color=LCD.black)
        show_time(LCD, hour_adj=hour_adj + 1, minute_adj=minute_adj + 1, blink=False, y_adj=50, color=LCD.gray)
        LCD.rect(20 + 120 * adjust_rect, 42, 78, 150, LCD.gray, False)
        LCD.show()

        x, y = Touch.X_point, Touch.Y_point
        # up
        if Touch.Gestures == 0x01:
            if adjust_rect == 0:
                hour_adj += 1
            else:
                minute_adj += 1
            print(f'up, {x=}')
            Touch.Gestures = 'None'
        # down
        elif Touch.Gestures == 0x02:
            if adjust_rect == 0:
                hour_adj -= 1
            else:
                minute_adj -= 1
            print(f'down, {x=}')
            Touch.Gestures = 'None'
        # left
        elif Touch.Gestures == 0x03:
            adjust_rect = 0
            print(f'left, {x=}')
            Touch.Gestures = 'None'
        # right
        elif Touch.Gestures == 0x04:
            adjust_rect = 1
            print(f'right, {x=}')
            Touch.Gestures = 'None'

    print('finish adjust')
    Touch.Gestures = 'None'
    rtc = machine.RTC()
    #year, month, mday, hour, minute, second, weekday, yearday
    rtc.datetime((year, month, mday, 1, hour, minute, 0, 0))
    print(rtc.datetime())


def show_stopwatch(seconds, LCD):
    second_str = seconds % 60
    minutes = seconds // 60
    minute_str = minutes % 60
    hour = minutes // 60

    strout = f'{rjust(hour)}:{rjust(minute_str)}:{rjust(second_str)}'
    LCD.fill(LCD.white)
    LCD.write_text(strout, 19, 100, 3, LCD.black)
    LCD.show()


def stopwatch(LCD, Touch):
    Touch.Gestures = "None"
    begin_time = 0
    current_time = 0
    pause = False

    # not long press
    while Touch.Gestures != 0x0C:
        # double press
        if Touch.Gestures == 0xB:
            begin_time = 0
            pause = False
            Touch.Gestures = "None"
        # single press
        if Touch.Gestures == 0x05:
            if begin_time == 0:
                begin_time = time.time()
                pause = False
            else:
                pause = True

            Touch.Gestures = "None"

        if begin_time == 0:
            show_stopwatch(0, LCD)
        else:
            if not pause:
                current_time = time.time()
                show_stopwatch(current_time - begin_time, LCD)

    print('finish stop watch')
    Touch.Gestures = 'None'

