from drive import LCD_1inch28, Touch_CST816T
from fuc import show_time, adjust_time, stopwatch
import time


if __name__ == "__main__":
    LCD = LCD_1inch28()
    LCD.set_bl_pwm(65535)
    Touch = Touch_CST816T(mode=1, LCD=LCD)
    Touch.Mode = 0
    Touch.Set_Mode(Touch.Mode)

    while 1:
        # long press
        if Touch.Gestures == 0x0C:
            print('pressed to adjust')
            adjust_time(LCD, Touch)
        # double press
        elif Touch.Gestures == 0x0B:
            stopwatch(LCD, Touch)
        else:
            LCD.fill(LCD.white)
            show_time(LCD)
            LCD.show()
            time.sleep(1)

